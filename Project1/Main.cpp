#include <unordered_map>
#include <iostream>
#include <vector>
#include <string_view>
#include <cctype>

template <class T, class K>
std::unordered_map<T, K> reverse_map(std::unordered_map<K, T> map)
{
    std::unordered_map<T, K> result;
    for (const auto& p : map)
    {
        result[p.second] = p.first;
    }

    return result;
}

enum TokenKind {
    k_SOME,
    k_WOWW,
    k_LOAD,
    k_OPEN_PAREN,
};

static std::unordered_map<TokenKind, std::string> token_kind_literal = {
    { TokenKind::k_SOME, "some" },
    { TokenKind::k_WOWW, "woww" },
    { TokenKind::k_LOAD, "load" },

    { TokenKind::k_OPEN_PAREN, "(" },
};
static std::unordered_map<std::string, TokenKind> token_kind_literal_r = reverse_map(token_kind_literal);


static std::vector<TokenKind> vec = {
    TokenKind::k_LOAD
};

TokenKind get_token()
{
    return TokenKind::k_LOAD;
}

int test_init_str()
{
    char c = '(';

    std::string cs(1, c);

    if (token_kind_literal_r.find(cs) == token_kind_literal_r.end())
    {
        std::cerr << "Key not found: " << cs << std::endl;
        return -1;
    }

    std::cout << token_kind_literal_r[cs] << std::endl;

    return 0;
}

int test_concat_str()
{
    std::string some = std::string() + 'c' + '2';

    std::cerr << some;

    return 0;
}

int test_cctype()
{
    char target = '0';

    int result = isalpha(target);
    
    if (result)
        std::cout << target << " is alpha!" << std::endl;
    else
        std::cout << target << " is not alpha!" << std::endl;

    return 0;
}

int cctype_note()
{
    isalnum('0');   // is either alphabet or numeric?
    isdigit('0');   // is digit?
    isxdigit('0');  // hex digit?

    return 0;
}

int test_hex()
{
    int x;
    std::cin >> std::hex >> x;
    std::cout << x << std::endl;

    return 0;
}

int main()
{
    //return test_init_str();
    //return test_concat_str();
    //return test_cctype();
    return test_hex();
}
